// Copyright (c) 2016, Alexey Safronov All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:

// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.

// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.

// 3. Neither the name of Rubicon-I nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.

// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

`timescale 1ns / 1ps

/*
    This module prevents the current sources from being powered on together. 
    That is, only one current source should be on at a time.
*/

module current_protector(
    // inputs
    input [3:0] D,
    input rst,

    // outputs
    output reg [3:0] Y
    );

    always @* begin
        if ( !rst ) begin
            Y <= 4'b0000;
        end
        else begin
            case ( D )
                4'b0000:
                    Y <= 4'b0000;
                4'b0001:
                    Y <= 4'b0001;
                4'b0010:
                    Y <= 4'b0010;
                4'b0011:
                    Y <= 4'b0010;
                4'b0100:
                    Y <= 4'b0100;
                4'b0101:
                    Y <= 4'b0100;
                4'b0110:
                    Y <= 4'b0100;
                4'b0111:
                    Y <= 4'b0100;
                4'b1000:
                    Y <= 4'b1000;
                4'b1001:
                    Y <= 4'b1000;
                4'b1010:
                    Y <= 4'b1000;
                4'b1011:
                    Y <= 4'b1000;
                4'b1100:
                    Y <= 4'b1000;
                4'b1101:
                    Y <= 4'b1000;
                4'b1110:
                    Y <= 4'b1000;
                4'b1111:
                    Y <= 4'b1000;
            endcase
        end
    end
endmodule
