// Copyright (c) 2016, Alexey Safronov All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:

// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.

// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.

// 3. Neither the name of Rubicon-I nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.

// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

`timescale 1ns / 1ps

module at_worker(
    // inputs
	input wire sys_clk,
	input wire rst,
    input wire at_work,

    // outputs
    output reg addr,
    output reg WR
    );


	reg at_work_rise, at_work_fall;
	reg at_work_before;
	reg trigger;

	task refresh;
		begin
			at_work_rise   <= at_work && !at_work_before;
			at_work_fall   <= !at_work && at_work_before;
		end
	endtask
	
	task swap;
		begin 
			at_work_before <= at_work;
		end
	endtask

	always @ (posedge sys_clk or negedge rst) begin
		if (!rst) begin
			// reset
			addr <= 0;
			WR <= 0;
			trigger <= 0;
			at_work_rise <= 0;
			at_work_fall <= 0;
			at_work_before <= 0;
		end
		else begin
			refresh;
			swap;
			
			if (trigger) begin
				addr <= 0;
				WR <= 0;
				trigger <= 0;
			end
			
			if (at_work_rise)
				addr <= 1;
			
			if (at_work_fall) begin
				WR <= 1;
				trigger <= 1;
			end
		end
		
	end

endmodule
