# set here the project name (i.e. the name of top-level module, e.g. midd126)
PROJECT  = stand_kpa_us17a

# set here the full name of the FPGA part (e.g. xc3s50antqg144-4)
PART     = xc3s50antqg144-4

# set here the short name of the FPGA part (e.g. xc3s50an)
PART_S   = xc3s50an

# set here the directory with your custom OPT files
OPT_DIR  = ../../../../../../../../../../projects/hdl/design_and_reuse/opts/

# set here the opt file for xst synthesis
XST_OPT  = my_xst_mixed.opt

# set here the opt file for the implementation
IMP_OPT  = my_balanced.opt

# set here the cleaning command
CLEANING = del /F /Q xst _xmsgs *.his *.bat *.opt *.xrpt *.scr *.lso *.flw \
           xlnx* *.lst *.pad *.pcf *.ptwx *.twr *.twx *.unroutes \
           *.xpi top_pad* xflow* *.xml *.bgn *.drc *.ll *.msk \
           *.xwbt *.html *.xrpt *.cfi *.prm _impact* webtalk* *.txt \
           *.ngm *.csv *.mrp

synth: $(PROJECT).prj 
	xflow -p $(PART) -synth $(OPT_DIR)/$(XST_OPT) $(PROJECT).prj 
	$(CLEANING)

implement: synth
	xflow -p $(PART) -implement $(OPT_DIR)/$(IMP_OPT) $(PROJECT).ngc
	$(CLEANING)

config: implement
	xflow -p $(PART) -config bitgen.opt $(PROJECT).ncd
	$(CLEANING)

mcs: config
	promgen -w -u 0 $(PROJECT)
	$(CLEANING) *.bit *.ncd *.ngd

impact: mcs
	impact -batch impact.cmd
	$(CLEANING) *.bit *.ncd *.ngd