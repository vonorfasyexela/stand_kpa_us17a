// Copyright (c) 2016, Alexey Safronov All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:

// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.

// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.

// 3. Neither the name of Rubicon-I nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.

// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

`timescale 1ns / 1ps

module stand_kpa_us17a(

    // inputs
    input external_sys_clk,
    input rst,
    input FPGA_MOSI,
    input FPGA_SCLK,
    input FPGA_CS,

    input [3:0] adc_select,
    input adc_cs,

    input [4:0] auto_xs1,
    input [4:0] auto_xs2,
    input [4:0] auto_xs3,
    input [4:0] auto_xs4,

    input i2c_miso,

    // outputs
    output FPGA_MISO,
    output [15:0] adc_dec,

    output [3:0] inc,
    output reset_m,

    output curr_en,
    output curr_mitd_en,
    output curr3,
    output curr2,
    output curr1,

    output i2c_mosi,
    output i2c_mosi_en,
    output i2c_clk,
    output i2c_clk_en

	);

	// #     # ### ######  #######  #####
	// #  #  #  #  #     # #       #     #
	// #  #  #  #  #     # #       #
	// #  #  #  #  ######  #####    #####
	// #  #  #  #  #   #   #             #
	// #  #  #  #  #    #  #       #     #
	//  ## ##  ### #     # #######  #####

	// =======================================================================
	wire dcm_feedback;
	wire rst_inv;
	wire clk_50mhz;
	wire clk_25mhz;
	wire clk_50mhz_unbuf;
	wire clk_25mhz_unbuf;
	wire [7:0] data_in;
	wire [4:0] reg_address;
	wire [7:0] data;
	wire WR;
	wire [31:0] CE;

	wire [7:0] module_reg_output;
	assign 
		inc = module_reg_output[6:3];
	assign
		reset_m = module_reg_output[2];

	wire [7:0] current_reg_output;
	wire [3:0] currents;
	assign curr_en = current_reg_output[4];
	assign curr_mitd_en = currents[3];
	assign curr3 = currents[2];
	assign curr2 = currents[1];
	assign curr1 = currents[0];

	wire [7:0] i2c_first_byte_reg_output;
	wire [7:0] i2c_secont_byte_reg_output;
	wire [7:0] i2c_count_byte_reg_output;
	wire [7:0] tx_mem_addr_reg_output;
	wire tx_mem_wea;
	wire [7:0] read_tx_mem_reg_output;
	wire [7:0] write_tx_mem_reg_output;
	wire [7:0] rx_mem_addr_reg_output;
	wire [7:0] read_rx_mem_reg_output;
	wire [7:0] i2c_control_reg_output;
	wire [7:0] speed_reg_output;
    wire [7:0] reg17_output;
	wire [7:0] reg18_output;
	wire [7:0] reg19_output;
	wire [7:0] reg20_output;
	wire [7:0] reg21_output;
	wire [7:0] reg22_output;
	wire [7:0] reg23_output;
	wire [7:0] reg24_output;
	wire [7:0] reg25_output;
	wire [7:0] reg26_output;
	wire [7:0] reg27_output;
	wire [7:0] reg28_output;
	wire [7:0] reg29_output;
	wire [7:0] reg30_output;
	wire [7:0] reg31_output;

	wire [7:0] ident;

	wire constant_1bit_0_output;
	wire [7:0] constant_8bit_0_output;
	wire constant_1bit_1_output;

	wire [7:0] tx_address;
	wire [7:0] data_for_transmit;
	wire rx_wr;
	wire [7:0] rx_data;
	wire [7:0] rx_address;
	wire at_work;

	wire at_work_address;
	wire at_work_wr;
	wire i2c_control_reg_input;
	wire i2c_control_reg_wr;
	wire i2c_control_reg_ce;

	// =======================================================================


	// ######  #######  #####  ###  #####  ####### ####### ######   #####
	// #     # #       #     #  #  #     #    #    #       #     # #     #
	// #     # #       #        #  #          #    #       #     # #
	// ######  #####   #  ####  #   #####     #    #####   ######   #####
	// #   #   #       #     #  #        #    #    #       #   #         #
	// #    #  #       #     #  #  #     #    #    #       #    #  #     #
	// #     # #######  #####  ###  #####     #    ####### #     #  #####

	// =======================================================================

	mux32_by8 _REG_MUX(
	    .D0(ident),
		.D1(module_reg_output),
		.D2(current_reg_output),
		.D3(auto_xs1),
		.D4(auto_xs2),
		.D5(auto_xs3),
		.D6(auto_xs4),
		.D7(i2c_first_byte_reg_output),
		.D8(i2c_secont_byte_reg_output),
		.D9(i2c_count_byte_reg_output),
		.D10(tx_mem_addr_reg_output),
		.D11(read_tx_mem_reg_output),
		.D12(write_tx_mem_reg_output),
		.D13(rx_mem_addr_reg_output),
		.D14(read_rx_mem_reg_output),
		.D15(i2c_control_reg_output),
		.D16(speed_reg_output),
		.D17(reg17_output),
		.D18(reg18_output),
		.D19(reg19_output),
		.D20(reg20_output),
		.D21(reg21_output),
		.D22(reg22_output),
		.D23(reg23_output),
		.D24(reg24_output),
		.D25(reg25_output),
		.D26(reg26_output),
		.D27(reg27_output),
		.D28(reg28_output),
		.D29(reg29_output),
		.D30(reg30_output),
		.D31(reg31_output),
	    
	    .address(reg_address),

	    .outp(data_in)
	);

	dec5_to_32 _ADDRESS_DECODER(
	    .enc(reg_address),

	    .dec(CE)
	);

	id _ID(
	    .ident(ident)
	);

	reg8 _MODULE_REG(
	    .inp(data),
	    .WR(WR),
	    .CE(CE[1]),
        .rst(rst),

	    .outp(module_reg_output)
	);	

	reg8 _CURRENT_REG(
	    .inp(data),
	    .WR(WR),
	    .CE(CE[2]),
        .rst(rst),

	    .outp(current_reg_output)
	);	

	current_protector _CURRENT_PROTECTOR(
	    .D(current_reg_output[3:0]),
	    .rst(rst),

	    .Y(currents)
	);

	reg8 _I2C_FIRST_BYTE_REG(
	    .inp(data),
	    .WR(WR),
	    .CE(CE[7]),
        .rst(rst),

	    .outp(i2c_first_byte_reg_output)
	);	

	reg8 _I2C_SECOND_BYTE_REG(
	    .inp(data),
	    .WR(WR),
	    .CE(CE[8]),
        .rst(rst),

	    .outp(i2c_secont_byte_reg_output)
	);	

	reg8 _I2C_COUNT_BYTE_REG(
	    .inp(data),
	    .WR(WR),
	    .CE(CE[9]),
        .rst(rst),

	    .outp(i2c_count_byte_reg_output)
	);	

	reg8 _TX_MEM_ADDR_REG(
	    .inp(data),
	    .WR(WR),
	    .CE(CE[10]),
        .rst(rst),

	    .outp(tx_mem_addr_reg_output)
	);	

	reg8 _WRITE_TX_MEM_REG(
	    .inp(data),
	    .WR(WR),
	    .CE(CE[12]),
        .rst(rst),

	    .outp(write_tx_mem_reg_output)
	);	

	reg8 _RX_MEM_ADDR_REG(
	    .inp(data),
	    .WR(WR),
	    .CE(CE[13]),
        .rst(rst),

	    .outp(rx_mem_addr_reg_output)
	);	

	reg8 _I2C_CONTROL_REG(
	    .inp(i2c_control_reg_input),
	    .WR(i2c_control_reg_wr),
	    .CE(i2c_control_reg_ce),
        .rst(rst),

	    .outp(i2c_control_reg_output)
	);	

    reg8 _I2C_SPEED_REG(
        .inp(data),
        .WR(WR),
        .CE(CE[16]),
        .rst(rst),

        .outp( speed_reg_output )
    );  

	reg8 _REG17(
	    .inp(data),
	    .WR(WR),
	    .CE(CE[17]),
        .rst(rst),

	    .outp( reg17_output )
	);	

	reg8 _REG18(
	    .inp(data),
	    .WR(WR),
	    .CE(CE[18]),
        .rst(rst),

	    .outp(reg18_output)
	);	

	reg8 _REG19(
	    .inp(data),
	    .WR(WR),
	    .CE(CE[19]),
        .rst(rst),

	    .outp(reg19_output)
	);	

	reg8 _REG20(
	    .inp(data),
	    .WR(WR),
	    .CE(CE[20]),
        .rst(rst),

	    .outp(reg20_output)
	);	

	reg8 _REG21(
	    .inp(data),
	    .WR(WR),
	    .CE(CE[21]),
        .rst(rst),

	    .outp(reg21_output)
	);	

	reg8 _REG22(
	    .inp(data),
	    .WR(WR),
	    .CE(CE[22]),
        .rst(rst),

	    .outp(reg22_output)
	);	

	reg8 _REG23(
	    .inp(data),
	    .WR(WR),
	    .CE(CE[23]),
        .rst(rst),

	    .outp(reg23_output)
	);	

	reg8 _REG24(
	    .inp(data),
	    .WR(WR),
	    .CE(CE[24]),
        .rst(rst),

	    .outp(reg24_output)
	);	

	reg8 _REG25(
	    .inp(data),
	    .WR(WR),
	    .CE(CE[25]),
        .rst(rst),

	    .outp(reg25_output)
	);	

	reg8 _REG26(
	    .inp(data),
	    .WR(WR),
	    .CE(CE[26]),
        .rst(rst),

	    .outp(reg26_output)
	);	

	reg8 _REG27(
	    .inp(data),
	    .WR(WR),
	    .CE(CE[27]),
        .rst(rst),

	    .outp(reg27_output)
	);	

	reg8 _REG28(
	    .inp(data),
	    .WR(WR),
	    .CE(CE[28]),
        .rst(rst),

	    .outp(reg28_output)
	);	

	reg8 _REG29(
	    .inp(data),
	    .WR(WR),
	    .CE(CE[29]),
        .rst(rst),

	    .outp(reg29_output)
	);	

	reg8 _REG30(
	    .inp(data),
	    .WR(WR),
	    .CE(CE[30]),
        .rst(rst),

	    .outp(reg30_output)
	);	

	reg8 _REG31(
	    .inp(data),
	    .WR(WR),
	    .CE(CE[31]),
        .rst(rst),

	    .outp(reg31_output)
	);	

	// #     # ####### #     # ####### ######  #     #
	// ##   ## #       ##   ## #     # #     #  #   #
	// # # # # #       # # # # #     # #     #   # #
	// #  #  # #####   #  #  # #     # ######     #
	// #     # #       #     # #     # #   #      #
	// #     # #       #     # #     # #    #     #
	// #     # ####### #     # ####### #     #    #
	// =======================================================================
	and_2in _TX_MEM_AND(
	    .i0(CE[12]),
	    .i1(WR),

	    .o(tx_mem_wea)
	);

	my_constant #(1, 1'b0) my_constant_1bit_0 (
	    .o(constant_1bit_0_output)
	);

	my_constant #(8, 8'b00000000) my_constant_8bit_0 (
		.o(constant_8bit_0_output)
	);

	memory _TX_MEMORY(
	    .clka(clk_25mhz),
	    .rsta(rst_inv),
	    .wea(tx_mem_wea),
	    .addra(tx_mem_addr_reg_output),
	    .dina(data),

	    .douta(read_tx_mem_reg_output),

	    .clkb(clk_25mhz),
	    .rstb(rst_inv),
	    .web(constant_1bit_0_output),
	    .addrb(tx_address),
	    .dinb(constant_8bit_0_output),

	    .doutb(data_for_transmit)
	);

	memory _RX_MEMORY(
	    .clka(clk_25mhz),
	    .rsta(rst_inv),
	    .wea(constant_1bit_0_output),
	    .addra(rx_mem_addr_reg_output),
	    .dina(constant_8bit_0_output),

	    .douta(read_rx_mem_reg_output),

	    .clkb(clk_25mhz),
	    .rstb(rst_inv),
	    .web(rx_wr),
	    .addrb(rx_address),
	    .dinb(rx_data),

	    .doutb()
	);

	// =======================================================================


	//  #####  ######  ###     #####  #          #    #     # #######
	// #     # #     #  #     #     # #         # #   #     # #
	// #       #     #  #     #       #        #   #  #     # #
	//  #####    ####   #      #####  #       #     # #     # #####
	//       # #        #           # #       #######  #   #  #
	// #     # #        #     #     # #       #     #   # #   #
	//  #####  #       ###     #####  ####### #     #    #    #######

	// =======================================================================
	spi_slave _SPI_SLAVE(
	    .MOSI(FPGA_MOSI),
	    .SCLK(FPGA_SCLK),
	    .CS(FPGA_CS),
	    .rst(rst),
	    .sys_clk(clk_25mhz),
	    .data_for_sending(data_in),

	    .MISO(FPGA_MISO),
	    .reg_address(reg_address),
	    .received_data(data),
	    .WR(WR)
	);
	// =======================================================================

	// ###  #####   #####     #     #    #     #####  ####### ####### ######
	//  #  #     # #     #    ##   ##   # #   #     #    #    #       #     #
	//  #        # #          # # # #  #   #  #          #    #       #     #
	//  #   #####  #          #  #  # #     #  #####     #    #####   ######
	//  #  #       #          #     # #######       #    #    #       #   #
	//  #  #       #     #    #     # #     # #     #    #    #       #    #
	// ### #######  #####     #     # #     #  #####     #    ####### #     #
	// =======================================================================
	i2c_master_statmach _I2C_MASTER_STATMACH(
	    .sys_clk(clk_50mhz),
	    .rst(rst),
	    .first_byte(i2c_first_byte_reg_output),
	    .second_byte(i2c_secont_byte_reg_output),
	    .data_for_transmit(data_for_transmit),
	    .count_byte(i2c_count_byte_reg_output),
	    .make_it(i2c_control_reg_output[0]),
        .speed( speed_reg_output[1:0] ),
	    .MISO(i2c_miso),

	    .tx_address(tx_address),
	    .rx_data(rx_data),
	    .rx_address(rx_address),
	    .rx_wr(rx_wr),
	    .at_work(at_work),
	    .MOSI(i2c_mosi),
	    .MOSI_EN(i2c_mosi_en),
	    .CLK(i2c_clk),
	    .CLK_EN(i2c_clk_en)
	    // .status(i2c_status)
	);

	at_worker _AT_WORKER(
	    .sys_clk(clk_50mhz),
	    .rst(rst),
	    .at_work(at_work),

	    .addr(at_work_address),
	    .WR(at_work_wr)
	);	

	mux_2in_by8 _AT_WORK_DATA_MUX(
	    .i0(data),
	    .i1(constant_8bit_0_output),
	    .sel(at_work_address),

	    .o(i2c_control_reg_input)
	);

	mux_2in_by1 _WR_MUX(
	    .i0(WR),
	    .i1(at_work_wr),
	    .sel(at_work_address),

	    .o(i2c_control_reg_wr)
	);

	mux_2in_by1 _CE_MUX(
	    .i0(CE[15]),
	    .i1(constant_1bit_1_output),
	    .sel(at_work_address),

	    .o(i2c_control_reg_ce)
	);	

	my_constant #( 1, 1'b1 ) my_constant_1bit_1(
	    .o(constant_1bit_1_output)
	);

	// =======================================================================

	//    #    ######   #####     ######  ####### #     # #     # #     #
	//   # #   #     # #     #    #     # #       ##   ## #     #  #   #
	//  #   #  #     # #          #     # #       # # # # #     #   # #
	// #     # #     # #          #     # #####   #  #  # #     #    #
	// ####### #     # #          #     # #       #     # #     #   # #
	// #     # #     # #     #    #     # #       #     # #     #  #   #
	// #     # ######   #####     ######  ####### #     #  #####  #     #

	// =======================================================================
	demux4_to16 _ADC_DEMUX(
	    .data(adc_cs),
	    .addr(adc_select),

	    .outp(adc_dec)
	);
	// =======================================================================


	//  #####  #     #  #####  ####### ####### #     #
	// #     #  #   #  #     #    #    #       ##   ##
	// #         # #   #          #    #       # # # #
	//  #####     #     #####     #    #####   #  #  #
	//       #    #          #    #    #       #     #
	// #     #    #    #     #    #    #       #     #
	//  #####     #     #####     #    ####### #     #

	// =======================================================================
	BUFG _CLK_BUFG(
	    .I(external_sys_clk),

	    .O(external_sys_clk_bufg)
	);

	BUFG _CLK1_BUFG(
	    .I(clk_50mhz_unbuf),

	    .O(clk_50mhz)
	);

	BUFG _CLK2_BUFG(
	    .I(clk_25mhz_unbuf),

	    .O(clk_25mhz)
	);

	invertor _RST_INVERTOR(
	    .i(rst),

	    .o(rst_inv)
	);

	DCM_SP #(
		.CLKDV_DIVIDE( 2.0 ), // Divide by: 1.5,2.0,2.5,3.0,3.5,4.0,4.5,5.0,5.5,6.0,6.5
		               //   7.0,7.5,8.0,9.0,10.0,11.0,12.0,13.0,14.0,15.0 or 16.0
		.CLKFX_DIVIDE( 8 ),   // Can be any integer from 1 to 32
		.CLKFX_MULTIPLY(2), // Can be any integer from 2 to 32
		.CLKIN_DIVIDE_BY_2("FALSE"), // TRUE/FALSE to enable CLKIN divide by two feature
		.CLKIN_PERIOD(10.0),  // Specify period of input clock
		.CLKOUT_PHASE_SHIFT("NONE"), // Specify phase shift of NONE, FIXED or VARIABLE
		.CLK_FEEDBACK("1X"),  // Specify clock feedback of NONE, 1X or 2X
		.DESKEW_ADJUST("SYSTEM_SYNCHRONOUS"), // SOURCE_SYNCHRONOUS, SYSTEM_SYNCHRONOUS or
		                                 //   an integer from 0 to 15
		.DLL_FREQUENCY_MODE("LOW"),  // HIGH or LOW frequency mode for DLL
		.DUTY_CYCLE_CORRECTION("TRUE"), // Duty cycle correction, TRUE or FALSE
		.PHASE_SHIFT(0),     // Amount of fixed phase shift from -255 to 255
		.STARTUP_WAIT("FALSE")   // Delay configuration DONE until DCM LOCK, TRUE/FALSE
	) DCM_SP_inst (
		.CLK0(dcm_feedback),     // 0 degree DCM CLK output
		.CLK180(), // 180 degree DCM CLK output
		.CLK270(), // 270 degree DCM CLK output
		.CLK2X(),   // 2X DCM CLK output
		.CLK2X180(), // 2X, 180 degree DCM CLK out
		.CLK90(),   // 90 degree DCM CLK output
		.CLKDV(clk_50mhz_unbuf),   // Divided DCM CLK out (CLKDV_DIVIDE)
		.CLKFX(clk_25mhz_unbuf),   // DCM CLK synthesis out (M/D)
		.CLKFX180(), // 180 degree CLK synthesis out
		.LOCKED(), // DCM LOCK status output
		.PSDONE(), // Dynamic phase adjust done output
		.STATUS(), // 8-bit DCM status bits output
		.CLKFB(dcm_feedback),   // DCM clock feedback
		.CLKIN(external_sys_clk_bufg),   // Clock input (from IBUFG, BUFG or DCM)
		.PSCLK(),   // Dynamic phase adjust clock input
		.PSEN(),     // Dynamic phase adjust enable input
		.PSINCDEC(), // Dynamic phase adjust increment/decrement
		.RST( rst_inv )        // DCM asynchronous reset input
	);
	// =======================================================================

endmodule